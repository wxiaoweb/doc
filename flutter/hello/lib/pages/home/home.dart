import 'package:flutter/material.dart';
import 'package:hello/models/home_model.dart';
import '../../components/star_rating.dart';
import '../../components/dash_line.dart';
import '../../network/home_request.dart';
import '../../theme/theme.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppbarColor,
        title: const Text("首页"),
      ),
      body: const Center(
        child: HomeContent(),
      ),
    );
  }
}

class HomeContent extends StatefulWidget {
  const HomeContent({Key? key}) : super(key: key);

  @override
  State<HomeContent> createState() => _HomeContentState();
}

class _HomeContentState extends State<HomeContent> {
  List<MovieItem> _movies = [];

  @override
  void initState() {
    super.initState();

    HomeRequest.getMovieTopList(0, 20).then((value) {
      setState(() {
        _movies.addAll(value);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(10),
      children: List.generate(
        _movies.length,
        (index) {
          // print(_movies[index].coverUrl);
          return MovieItemWidget(movie: _movies[index]);
        },
      ),
    );
  }
}

class MovieItemWidget extends StatelessWidget {
  final MovieItem movie;

  const MovieItemWidget({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _genRank(),
        // const SizedBox(height: 10),
        Container(
          // height: 250,
          margin: const EdgeInsets.fromLTRB(0, 10, 0, 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Image.network("https://cdn.pixabay.com/photo/2016/02/09/19/57/aurora-1190254_1280.jpg"),
              ClipRRect(
                borderRadius: BorderRadius.circular(5),
                // child: Image.network(movie.coverUrl ?? ""),
                child: Image.network(
                  "https://cdn.pixabay.com/photo/2016/02/09/19/57/aurora-1190254_1280.jpg",
                  // movie.coverUrl ?? "https://cdn.pixabay.com/photo/2016/02/09/19/57/aurora-1190254_1280.jpg",
                  width: 100,
                  height: 150,
                  fit: BoxFit.fill,
                ),
              ),

              Expanded(
                child: Container(
                  // height: 150,
                  padding:
                      const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                  constraints: const BoxConstraints(
                    maxHeight: 150,
                    // maxWidth: 250,
                    // minWidth: 150,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getTitleWidget(),
                      ...getContentWidget(),
                      const SizedBox(height: 8),
                      getRatingWidget(),
                      const SizedBox(height: 3),
                    ],
                  ),
                ),
              ),

              getDashLine(),
              getContentWish(),
            ],
          ),
        )
      ],
    );
  }

  Widget _genRank() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(3),
        color: const Color.fromARGB(255, 238, 205, 144),
      ),
      padding: const EdgeInsets.fromLTRB(9, 4, 9, 4),
      child: Text(
        "NO.${movie.rank ?? 0}",
        style: const TextStyle(
            fontSize: 18, color: Color.fromARGB(255, 131, 95, 36)),
      ),
    );
  }

  Widget getTitleWidget() {
    return Stack(
      children: <Widget>[
        const Icon(
          Icons.play_circle_outline,
          color: Colors.redAccent,
        ),
        Text.rich(
          TextSpan(children: [
            TextSpan(
                text: "     " + (movie.title ?? ""),
                style:
                    const TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            TextSpan(
              text: "(${movie.releaseDate})",
              style: const TextStyle(fontSize: 18, color: Colors.black54),
            )
          ]),
          maxLines: 2,
        ),
      ],
    );
  }

  List<Widget> getContentWidget() {
    final date = movie.releaseDate ?? "1994";
    final year = date.split("-")[0];
    final desc = [year];
    desc.addAll(movie.regions ?? []);
    desc.addAll(movie.types ?? []);

    final actors = movie.actors?.join("/");
    final descStr = desc.join("/");

    return [
      Text(
        actors ?? "",
        style: const TextStyle(fontSize: 18, color: Colors.black54),
        overflow: TextOverflow.ellipsis,
      ),
      Text(
        descStr,
        style: const TextStyle(fontSize: 18, color: Colors.black54),
        overflow: TextOverflow.ellipsis,
      ),
    ];
  }

  Widget getRatingWidget() {
    double score = double.parse(movie.score ?? "0");
    return Row(
      // mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        StarRating(
          rating: score,
          maxRating: 10,
          imageSize: 18,
          unselectImage:
              const Icon(Icons.star, size: 18, color: Color(0xffbbbbbb)),
          selectedImage:
              const Icon(Icons.star, size: 18, color: Color(0xffe0aa46)),
        ),
        const SizedBox(width: 5),
        Text(movie.score ?? "")
      ],
    );
  }

  Widget getDashLine() {
    return Container(
      width: 2,
      height: 120,
      child: const DashLine(
        axis: Axis.vertical,
        color: Colors.grey,
        // dashWidth: 20,
        dashHeight: 20,
        dashCount: 3,
        // dashCount: 10,
      ),
    );
  }

  Widget getContentWish() {
    return Container(
      width: 60,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          const SizedBox(
            height: 20,
          ),
          Image.asset(
            "assets/images/home/wish.png",
            width: 30,
          ),
          const SizedBox(
            height: 5,
          ),
          const Text(
            "想看",
            style: TextStyle(
                fontSize: 16, color: Color.fromARGB(255, 235, 170, 60)),
          )
        ],
      ),
    );
  }

  // 电影简介（原生名称）
  Widget getMovieIntroduceWidget() {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(12),
      decoration: BoxDecoration(
          color: const Color(0xfff2f2f2),
          borderRadius: BorderRadius.circular(5)),
      child: Text(
        movie.url ?? "",
        style: const TextStyle(fontSize: 18, color: Colors.black54),
      ),
    );
  }
}

