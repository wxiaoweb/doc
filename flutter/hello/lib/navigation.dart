import 'package:flutter/cupertino.dart';

import './pages/home/home.dart';
import './pages/subject/subject.dart';
import './pages/group/group.dart';
import './pages/mall/mall.dart';
import './pages/profile/profile.dart';

class NavigationItem {
  final String title;
  final String icon;
  final Widget widget;

  NavigationItem(this.title, this.icon, this.widget);
}

var navigationList = [
  NavigationItem("首页", "home", const Home()),
  NavigationItem("书影音", "subject", const Subject()),
  NavigationItem("小组", "group", const Group()),
  NavigationItem("集市", "mall", const Mall()),
  NavigationItem("我de", "profile", const Profile())
];