
import 'package:flutter/material.dart';

class ThemeApp extends StatelessWidget {
  const ThemeApp({Key? key}):super(key: key);

  @override
  Widget build(BuildContext context) {

    // final dpr = window.devicePixelRatio;
    // final w = window.screen?.width ?? 0;

    // 状态
    // MediaQuery.of(context).padding.top / dpr;
    return MaterialApp(
      title: "Theme Demo",
      theme: ThemeData(
        brightness: Brightness.light,
        //
        primarySwatch: Colors.green,

        primaryColor: Colors.red,

        textTheme: const TextTheme(
          bodyText1: TextStyle(fontSize: 20),
        ),

        cardTheme: const CardTheme(
          color: Colors.black,
        ),
        tabBarTheme: const TabBarTheme(

        ),
        buttonTheme: const ButtonThemeData(
          minWidth: 10
        ),
      ),
    );
  }
}
