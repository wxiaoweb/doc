

import 'dart:ui';

class Media {
  static double physicalWidth = 0;
  static double physicalHeight = 0;
  static double screenWidth = 0;
  static double screenHeight = 0;
  static double dpr = 1;
  static double rpx = 1;
  static double px = 1;
  static double statusHeight = 12;

  static void init({double standardSize = 750}) {
    physicalWidth = window.physicalSize.width;
    physicalHeight = window.physicalSize.height;

    dpr = window.devicePixelRatio;

    screenWidth = physicalWidth / dpr;
    screenHeight = physicalHeight / dpr;

    rpx = screenWidth / standardSize;
    px = rpx * 2;

    statusHeight = window.padding.top / dpr;
  }
}

extension DoubleExt on double {
  double get rpx {
    return this * Media.rpx;
  }
}


extension IntExt on int {
  double get rpx {
    return toDouble() * Media.rpx;
  }
}