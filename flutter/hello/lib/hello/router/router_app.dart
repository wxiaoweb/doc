import 'package:flutter/material.dart';

import 'about_page.dart';
import 'detail_page.dart';

/*
*
* MaterialPageRoute: 用来包裹路由，不同平台不同表现
*   > IOS 也可以选择使用CupertinoPageRoute
*
* Navigator: 用于管理所有route，其内部维护一个stack管理所有widget(MaterialPageRoute)
* 普通跳转:
*   > Navigator.of(context).push();
*   > navigator.push(MaterialPageRoute(builder: (ctx)=>const DetailPage()));
*
* 路由跳转:
*   > navigator.pushNamed(AboutPage.routeName, arguments: "about param");
*   > final String args = ModalRoute.of(context)?.settings.arguments as String; 获取参数
*
* */

class RouterApp extends StatelessWidget {
  const RouterApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primarySwatch: Colors.green, splashColor: Colors.transparent),

      // 路由跳转
      routes: {
        RouterHomePage.routeName: (context) => const RouterHomePage(),
        AboutPage.routeName: (ctx) => const AboutPage(),
        DetailPage.routeName: (ctx) => const DetailPage(),
      },

      // home: const RouterHomePage(),
      initialRoute: RouterHomePage.routeName,

      // onGenerateInitialRoutes: (settings) {
      //   return [];
      // },

      // 当上面路由映射不存在的情况下，触发这里执行
      onGenerateRoute: (settings) {
        if (settings.name == "t") {
          return MaterialPageRoute(
            builder: (ctx) {
              // settings?.arguments
              return const RouterHomePage();
            },
          );
        }
      },

      // 对于不存在的路由
      onUnknownRoute: (settings) {
        // Unknown page
      },
    );
  }
}

class RouterHomePage extends StatefulWidget {
  static const String routeName = "/";

  const RouterHomePage({Key? key}) : super(key: key);

  @override
  State<RouterHomePage> createState() => _RouterHomePageState();
}

class _RouterHomePageState extends State<RouterHomePage> {
  var _data = "welcome ";
  var _about = "about";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("首页",
            style: TextStyle(color: Colors.white, fontSize: 40)),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
            // 或者使用WillPopScope，自己实现返回代码，替换默认的返回
            onPressed: () {
              _jumpToDetail(context);
            },
            child: Text("Detail: ${_data}",
                style: const TextStyle(color: Colors.white, fontSize: 30)),
          ),
          ElevatedButton(
            // 或者使用WillPopScope，自己实现返回代码，替换默认的返回
            onPressed: () {
              _jumpToAbout(context);
            },
            child: Text("About: ${_about}",
                style: const TextStyle(color: Colors.white, fontSize: 30)),
          ),
        ],
      )),
    );
  }

  _jumpToDetail(BuildContext context) {
    var navigator = Navigator.of(context);
    var feature =
        navigator.push(MaterialPageRoute(builder: (ctx) => const DetailPage()));
    feature.then((value) {
      setState(() {
        _data = value;
      });
    });
  }

  _jumpToAbout(BuildContext context) {
    var navigator = Navigator.of(context);
    var feature =
        navigator.pushNamed(AboutPage.routeName, arguments: "about param");
    // 页面pop之后会调用feature的then
    feature.then((value) {
      setState(() {
        _about = value as String;
      });
    });
  }
}
