import 'package:flutter/material.dart';
import 'package:hello/hello/router/router_app.dart';

// 优化代码
class WRouter {
  static final Map<String, WidgetBuilder> routes = {

  };

  static const String initialRoute = "";

  static final RouteFactory generateRoute = (settings) {
    if (settings.name == "t") {
      return MaterialPageRoute(
        builder: (ctx) {
          return const RouterHomePage();
        },
      );
    }
    return null;
  };

  static final RouteFactory unKnownRoute = (settings) {
    return null;
  };
}
