import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  static const String routeName = "/detail";

  const DetailPage({Key? key}) :super(key: key);

  @override
  Widget build(BuildContext context) {
    // WillPopScope (
    //   onWillPop: () {
    //     // true: 自动返回，
    //     // false: 自己代码实现返回
    //     return Future.value(true);
    //   },
    // )
    return Scaffold(
      appBar: AppBar(
        title: const Text("Detail", style: TextStyle(color: Colors.black, fontSize: 20)),
        leading: ElevatedButton(onPressed: (){_back(context);}, child: const Icon(Icons.back_hand),)
      ),

      body: Center(
        child: ElevatedButton(
            onPressed: () {
              _back(context);
            },
            child: const Text("详情内容。。。。",
              style: TextStyle(color: Colors.black, fontSize: 20),)),
      ),
    );
  }

  void _back(BuildContext context) {
    // pop携带返回参数
    Navigator.of(context).pop("pop from detail");
  }
}
