import 'dart:ui';

import 'package:flutter/material.dart';

class FitWidgetApp extends StatelessWidget {
  const FitWidgetApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // 物理分辨率
    final phyWidth = window.physicalSize.width;
    final phyHeight = window.physicalSize.height;

    // window size = window.physicalSize / window.devicePixelRatio,
    final winSize = (window.physicalSize / window.devicePixelRatio);
    final winHeight = winSize.height;
    final winWidth = winSize.width;

    // status tab
    final statusHeight = window.padding.top / window.devicePixelRatio;

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("fit diwget"),
        ),
        body: FitWidgetBody(),
      ),
    );
  }
}

class FitWidgetBody extends StatefulWidget {
  const FitWidgetBody({Key? key}) : super(key: key);

  @override
  State<FitWidgetBody> createState() => _FitWidgetBodyState();
}

class _FitWidgetBodyState extends State<FitWidgetBody> {
  @override
  Widget build(BuildContext context) {

    // 手机屏幕大小，需要win初始化之后才行
    final winWidth = MediaQuery.of(context).size.width;
    final winHeight = MediaQuery.of(context).size.height;
    // size = window.physicalSize / window.devicePixelRatio,

    return Text("body..");
  }
}
