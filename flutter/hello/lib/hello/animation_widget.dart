import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';

/*
*   动画
*   Animation
*     - addListener
*     - removeListener
*     - status
*     - value
*
*   子类
*     - AnimationController:继承自Animation
*         - vsync: 同步信号，方便下一次绘制
*         - forward()
*         - reverse()
*
*     - CurvedAnimation: 管理AnimationController, 决定执行速率，贝塞尔曲线
* 
*     - Tween: 执行范围
*     -
*
*
*   优化性能
*     - AnimatedWidget： 包装一下动画组件
*
*     - AnimatedBuilder:
* */

class AnimationWidgetPage extends StatelessWidget {
  const AnimationWidgetPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final controller = AnimationController(vsync: this);
    // final curved = CurvedAnimation(parent: controller, curve: Curves.elasticIn);
    // final aniValue = Tween(begin: 10, end: 200).animate(controller);
    // final aniValue2 = Tween(begin: 10, end: 200).animate(curved);

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text(
            "Animation 动画",
            style: TextStyle(fontSize: 30),
          ),
        ),
        body: const Center(
          child: MultiAnimationWidget(),
        ),
      ),
    );
  }
}

class HeartAnimationWidget extends StatefulWidget {
  const HeartAnimationWidget({Key? key}) : super(key: key);

  @override
  State<HeartAnimationWidget> createState() => _HeartAnimationWidgetState();
}

class _HeartAnimationWidgetState extends State<HeartAnimationWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late CurvedAnimation _curvedAnimation;
  late Animation<double> _tween;
  double _size = 50.0;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 1));

    _curvedAnimation =
        CurvedAnimation(parent: _controller, curve: Curves.elasticIn);

    _tween = Tween(begin: 50.0, end: 100.0).animate(_curvedAnimation);

    // 监听动画状态变化，但是setSate会导致重新build，有性能问题
    _controller.addListener(() {
      // setState(() {
      //   _size = _tween.value;
      // });
    });

    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _controller.forward();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    print("_HeartAnimationWidgetState build");
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // Icon(
          //   Icons.favorite,
          //   size: _size,
          //   color: Colors.red,
          // ),

          // FavorAnimatedWidget(
          //   listenable: _tween,
          // ),

          AnimatedBuilder(
            animation: _tween,
            builder: (BuildContext context, Widget? child) {
              print("AnimatedBuilder build");
              return Icon(
                Icons.favorite,
                size: _tween.value,
                color: Colors.red,
              );
            },
            child: null,
          ),

          FloatingActionButton(
            child: const Icon(Icons.play_arrow),
            onPressed: () {
              // _controller.forward();
              if (_controller.isAnimating) {
                _controller.stop();
              } else {
                _controller.forward();
              }
            },
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

// 初步优化动画
class FavorAnimatedWidget extends AnimatedWidget {
  FavorAnimatedWidget({required Listenable listenable})
      : super(listenable: listenable);

  @override
  Widget build(BuildContext context) {
    print("FavorAnimatedWidget build");
    Animation ani = listenable as Animation;
    return Icon(
      Icons.heart_broken,
      size: ani.value,
      color: Colors.red,
    );
  }
}

class MultiAnimationWidget extends StatefulWidget {
  const MultiAnimationWidget({Key? key}) : super(key: key);

  @override
  State<MultiAnimationWidget> createState() => _MultiAnimationWidgetState();
}

class _MultiAnimationWidgetState extends State<MultiAnimationWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late CurvedAnimation _curvedAnimation;
  late Animation _transAni;
  late Animation _scaleAni;
  late Animation _rotateAni;
  late Animation _colorAni;

  double _opacity = 0.5;
  double _scale = 1.0;
  double _rotate = pi / 4;
  Color _color = Colors.pink;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 1));
    _curvedAnimation =
        CurvedAnimation(parent: _controller, curve: Curves.linear);

    _transAni = Tween(begin: 0.0, end: 1.0).animate(_curvedAnimation);
    _scaleAni = Tween(begin: 0.0, end: 1.0).animate(_curvedAnimation);
    _rotateAni = Tween(begin: 0.0, end: pi).animate(_curvedAnimation);
    _colorAni = ColorTween(begin: Colors.pink, end: Colors.green)
        .animate(_curvedAnimation);

    _controller.addListener(() {
      setState(() {
        _opacity = _transAni.value;
        _scale = _scaleAni.value;
        _rotate = _rotateAni.value;
        _color = _colorAni.value;
      });
    });

    _controller.addStatusListener((status) {
      if (status == AnimationStatus.dismissed) {
        _controller.forward();
      } else if (status == AnimationStatus.completed) {
        _controller.reverse();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Opacity(
        opacity: _opacity,
        child: Transform(
          alignment: Alignment.center,
          transform: Matrix4.rotationZ(_rotate),
          child: GestureDetector(
            onTapDown: (event) {
              _controller.forward();

              Timer.periodic(Duration(seconds: 3), (timer) {
                timer.cancel();

                Navigator.of(context).push(
                  // 无动画效果
                  // MaterialPageRoute(
                  //   builder: (BuildContext context) {
                  //     return  SecondPage();
                  //   },
                  //   fullscreenDialog: false,
                  // ),

                  // 添加动画效果
                  PageRouteBuilder(
                    transitionDuration: const Duration(seconds: 3),

                    pageBuilder: (BuildContext context,
                        Animation<double> animation,
                        Animation<double> secondaryAnimation) {

                      return FadeTransition(
                        opacity: animation,
                        child: const SecondPage(),
                      );
                    },

                    fullscreenDialog: false,
                  ),
                );
              });
            },
            child: Container(
              width: 200.0 * _scale,
              height: 200.0 * _scale,
              color: _color,
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

class SecondPage extends StatelessWidget {
  const SecondPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Second page"),
      ),
      body: Center(
        child: Container(
          width: 400,
          height: 600,
          color: Colors.blue,
        ),
      ),
    );
  }
}
