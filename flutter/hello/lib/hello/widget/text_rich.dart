import 'package:flutter/material.dart';

class TextRichWidget extends StatelessWidget {
  final userNameController = TextEditingController();
  final passwordController = TextEditingController();

  // TextRichWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text.rich(
          TextSpan(children: [
            TextSpan(
                text: "Hello",
                style: TextStyle(fontSize: 20, color: Colors.red)),
            WidgetSpan(child: Icon(Icons.add_chart)),
            TextSpan(
                text: "World",
                style: TextStyle(fontSize: 50, color: Colors.green)),
          ]),
        ),
        TextField(
          controller: userNameController,
          decoration: const InputDecoration(
            // labelText: "please ...",
            icon: Icon(Icons.people),
            hintText: "please input usename",
            // border: OutlineInputBorder(),
            border: InputBorder.none,
          ),
          onChanged: (value) {},
          onSubmitted: (value) {},
        ),
        FlatButton(
          onPressed: () {
            print("username :${userNameController.text}");
          },
          child: Text("submit"),
        ),
      ],
    );
  }
}
