import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StateWidget extends StatelessWidget {
  const StateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return const CounterWidget();
    return const CountProviderParentWidget();
  }
}

// Inherited Share State
class CounterWidget extends StatefulWidget {
  const CounterWidget({Key? key}) : super(key: key);

  @override
  State<CounterWidget> createState() => _CounterWidgetState();
}

class _CounterWidgetState extends State<CounterWidget> {
  int _value = 0;

  @override
  void didUpdateWidget(covariant CounterWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    print("run didUpdateWidget--->$_value");
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("run didChangeDependencies--->$_value");
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          StateInheriteWidget(
            _value,
            child: ValueOneWidget(),
          ),
          StateInheriteWidget(
            _value,
            child: ValueTwoWidget(),
          ),
          FloatingActionButton(
            child: const Icon(
              Icons.add,
              size: 30,
            ),
            onPressed: () {
              setState(() {
                _value += 1;
              });
            },
          )
        ],
      ),
    );
  }
}

class StateInheriteWidget extends InheritedWidget {
  // define share value
  final int value;

  // define construct
  const StateInheriteWidget(this.value, {required Widget child})
      : super(child: child);

  // get current InheritedWidget
  static StateInheriteWidget? of(BuildContext ctx) {
    return ctx.dependOnInheritedWidgetOfExactType();
  }

  // if return true,
  @override
  bool updateShouldNotify(StateInheriteWidget oldWidget) {
    print("run updateShouldNotify--> ${oldWidget.value}, $value");
    return oldWidget.value != value;
  }
}

class ValueTwoWidget extends StatelessWidget {
  const ValueTwoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final statWidget = StateInheriteWidget.of(context);
    final _value = statWidget?.value;

    return Container(
      color: Colors.green,
      child: Text(
        "Value: $_value",
        style: const TextStyle(fontSize: 30),
      ),
    );
  }
}

class ValueOneWidget extends StatefulWidget {
  const ValueOneWidget({Key? key}) : super(key: key);

  @override
  State<ValueOneWidget> createState() => _ValueOneWidgetState();
}

class _ValueOneWidgetState extends State<ValueOneWidget> {
  int _value = 0;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("run ValueOneWidget::didChangeDependencies-->");
    final statWidget = StateInheriteWidget.of(context);
    setState(() {
      // if (statWidget != null) {
      //   _value = statWidget.value;
      // }
      _value = statWidget!.value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
      child: Text(
        "Value: $_value",
        style: const TextStyle(fontSize: 30),
      ),
    );
  }
}

// --------------------------> Provider <----------------------------------
/*
* 1-创建共享数据
* 2-使用数据的节点的根节点，创建provider
* 3-其他节点使用数据
*   - Provider.of：当Provider中数据发生改变，Provider.of所在的widget整个build方法回调用
*   - Consumer：当数据变化，只会执行Consumer中的builder
*   - Selector: selector方法对原有数据进行转化，然后给build使用
*               shouldRebuild方便可以先判断是否进行重新构建
* */
class ProviderCounterViewModel with ChangeNotifier {
  int _counter = 10;

  int get counter => _counter;

  set counter(int value) {
    _counter = value;
    notifyListeners();
  }
}

class ProviderUserViewModel with ChangeNotifier {
  String _name;

  ProviderUserViewModel(String name):_name=name;


  String get name => _name;

  set name(String value) {
    _name = value;
    notifyListeners();
  }
}

class ObjectContainer {
  ObjectContainer({required this.Counter, required this.User});
  ProviderCounterViewModel Counter;
  ProviderUserViewModel User;
}

class CountProviderParentWidget extends StatelessWidget {
  const CountProviderParentWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("CountProviderParentWidget run build");

    // Single
    // return ChangeNotifierProvider(
    //   create: (context) {
    //     return ProviderCounterViewModel();
    //   },
    //   child: const CountProviderWidget(),
    // );

    // Multi
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (ctx) =>ProviderCounterViewModel()),
          ChangeNotifierProvider(create: (ctx)=>ProviderUserViewModel("MultiProvider")),
          // 继续添加其他的provider
        ],
        child: const CountProviderWidget(),
    );
  }
}

class CountProviderWidget extends StatefulWidget {
  const CountProviderWidget({Key? key}) : super(key: key);

  @override
  State<CountProviderWidget> createState() => _CountProviderWidgetState();
}

class _CountProviderWidgetState extends State<CountProviderWidget> {
  @override
  Widget build(BuildContext context) {
    print("CountProviderWidget run build");
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          ProviderValueOne(),
          ProviderValueTwo(),
          ProviderValueThree(),
          ProviderFloatAction(),
        ],
      ),
    );
  }
}

class ProviderValueOne extends StatelessWidget {
  const ProviderValueOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("ProviderValueOne run build");
    Provider.debugCheckInvalidValueType = null;
    final pData = Provider.of<ProviderCounterViewModel>(context);
    return Container(
      color: Colors.red,
      child: Text(
        "Value: ${pData.counter}",
        style: const TextStyle(fontSize: 30),
      ),
    );
  }
}

class ProviderValueTwo extends StatefulWidget {
  const ProviderValueTwo({Key? key}) : super(key: key);

  @override
  State<ProviderValueTwo> createState() => _ProviderValueTwoState();
}

class _ProviderValueTwoState extends State<ProviderValueTwo> {
  // int _value = 0;
  @override
  Widget build(BuildContext context) {
    print("ProviderValueTwo run build");

    return Container(
      color: Colors.green,
      child: Consumer<ProviderCounterViewModel>(
        builder: (ctx, value, child) {
          print("ProviderValueTwo Consumer build");
          return Text(
            "Value:  ${value.counter}",
            style: const TextStyle(fontSize: 30),
          );
        },
      ),
    );
  }
}

class ProviderValueThree extends StatelessWidget {
  const ProviderValueThree({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("ProviderValueThree run build");
    return Consumer2<ProviderCounterViewModel, ProviderUserViewModel>(
      builder: (ctx, counterValue, userValue, child) {
        return Container(
          color: Colors.blue,
          child: Text(
            "User: ${userValue.name}, Value: ${counterValue.counter}",
            style: const TextStyle(fontSize: 30),
          ),
        );
      },
    );
  }
}

class ProviderFloatAction extends StatelessWidget {
  const ProviderFloatAction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("ProviderFloatAction build....");
    return Selector2<ProviderCounterViewModel, ProviderUserViewModel, ObjectContainer>(
      selector: (ctx, counterValue, userValue) {
        return ObjectContainer(Counter: counterValue, User: userValue);
      },
      shouldRebuild: (prev, next) => prev.Counter.counter != next.Counter.counter,
      builder: (context, value, child) {
        print("ProviderFloatAction Consumer build....$value");
        return FloatingActionButton(
          child: child,
          onPressed: () {
            value.Counter.counter += 1;
            value.User.name = "username-${value.Counter.counter}";
          },
        );
      },
      child: const Icon(
        Icons.add,
        size: 30,
      ),
    );

    // return Consumer<ProviderCounterViewModel>(
    //     builder: (context, value, child) {
    //       print("ProviderFloatAction Consumer build....${value.counter}");
    //       return FloatingActionButton(
    //         child: child,
    //         onPressed: () {
    //           value.counter += 1;
    //         },
    //       );
    //   },
    //
    //   child:  const Icon(
    //     Icons.add,
    //     size: 30,
    //   ),
    // );
  }
}
