import 'package:flutter/cupertino.dart';

class ImageWidget extends StatelessWidget {
  const ImageWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const space = SizedBox(height: 10);
    const url =
        "https://cdn.pixabay.com/photo/2016/02/09/19/57/aurora-1190254_1280.jpg";
    const url2 = "https://static.runoob.com/images/demo/demo2.jpg";
    const local = "assets/images/polor-light.jpg";

    return ListView(
      children: [
        FadeInImage.assetNetwork(placeholder: local, image: url2),
        const FadeInImage(
          fadeOutDuration: Duration(milliseconds: 3000),
          placeholder: AssetImage(local),
          image: NetworkImage(url2),
        ),

        const Image(
          image: NetworkImage(url),
        ),
        space,

        const Image(
          width: 150,
          height: 150,
          fit: BoxFit.fitHeight,
          alignment: Alignment.centerLeft,
          image: NetworkImage(url),
        ),
        space,

        //
        Image.network(url),
        space,

        const Image(
          image: AssetImage(local),
        ),
        space,

        Image.asset(local),
        space,
      ],
    );
  }
}
