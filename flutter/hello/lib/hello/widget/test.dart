import 'package:flutter/material.dart';
import 'package:hello/hello/widget/image_widget.dart';
import 'package:hello/hello/widget/state_widget.dart';

import './list_widget.dart';

class TestApp extends StatelessWidget {
  const TestApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Demo"),
        ),
        body: ListViewWidget(),
      ),
    );
  }
}
