import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  const ButtonWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        RaisedButton(
          onPressed: () {
            print("click button");
          },
          child: const Text("RaiseButton"),
        ),
        ButtonTheme(
          minWidth: 20,
          height: 10,
          child: FlatButton(
            color: Colors.yellow,
            onPressed: () {},
            child: Text("F"),
          ),
        ),
        ElevatedButtonTheme(
            data: ElevatedButtonThemeData(
              style: ButtonStyle(
                minimumSize: MaterialStateProperty.all(Size.zero),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                padding: MaterialStateProperty.all(const EdgeInsets.all(0)),
              ),
            ),
            child: ElevatedButton(
              onPressed: (){},
              child: const Text("ETT"),
            ),
        ),
        ElevatedButton(
          style: const ButtonStyle(
              tapTargetSize: MaterialTapTargetSize.shrinkWrap),
          onPressed: () {
            print("click elevated button1");
          },
          child: const Text("Elevated Button1"),
        ),
        ElevatedButton(
          style: const ButtonStyle(
              tapTargetSize: MaterialTapTargetSize.shrinkWrap),
          onPressed: () {
            print("click elevated button");
          },
          child: const Text("Elevated Button2"),
        ),
        FlatButton(
          onPressed: () {},
          child: const Text("flat button"),
        ),
        TextButton(
          onPressed: () {},
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
            shape: MaterialStateProperty.resolveWith(
              (states) => RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6),
              ),
            ),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: const [
              Icon(
                Icons.favorite,
                color: Colors.red,
              ),
              Text(
                "TextButton",
                style: TextStyle(color: Colors.black),
              ),
            ],
          ),
        ),
        OutlineButton(
          onPressed: () {},
          child: const Text("OUtling Button"),
        ),
        OutlinedButton(onPressed: () {}, child: const Text("OUtlined Button")),
        FloatingActionButton(
          onPressed: () {},
          child: const Icon(Icons.add),
        ),
      ],
    );
  }
}
