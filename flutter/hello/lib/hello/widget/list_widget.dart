import 'dart:math';
import 'package:flutter/material.dart';
import 'package:hello/hello/widget/image_widget.dart';

class ListViewWidget extends StatelessWidget {
  const ListViewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ListViewSimaple();
  }
}

class ListViewSimaple extends StatelessWidget {
  const ListViewSimaple({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return ListView(
    //   scrollDirection: Axis.vertical,
    //   // itemExtent: 10, //  固定高度
    //   // primary: ,
    //   children: List.generate(
    //     12,
    //     (index) {
    //       // return Text("hello ${index}");
    //       return ListTile(
    //         leading: Icon(Icons.people),
    //         trailing: Icon(Icons.delete),
    //         title: Text("hello ${index}"),
    //         subtitle: Text("sub---"),
    //       );
    //     },
    //   ),
    // );

    // return ListView.builder(
    //   itemCount: 10,
    //   itemBuilder: (BuildContext context, int index) {
    //     return Text("data...");
    //   },
    // );

    // return ListView.separated(
    //   itemBuilder: (BuildContext context, int index) {
    //     return const Text("data");
    //   },
    //   separatorBuilder: (BuildContext context, int index) {
    //     return const Divider(
    //       color: Colors.blue,
    //       thickness: 2,
    //       // height: 10,
    //     );
    //   },
    //   itemCount: 100,
    // );

    //--------------------------grid view---------------------------
    var random = Random();
    // return Padding(
    //   padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
    //   child: GridView(
    //     gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
    //       crossAxisCount: 4,
    //       mainAxisSpacing: 8,
    //       crossAxisSpacing: 8,
    //     ),
    //     children: List.generate(50, (index) {
    //       return Container(
    //         color: Color.fromARGB(255, random.nextInt(256), random.nextInt(256), random.nextInt(256)),
    //       );
    //     },),
    //   ),
    // );

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
      child: GridView(
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
          mainAxisSpacing: 8,
          crossAxisSpacing: 8,
          maxCrossAxisExtent: 200,
        ),
        children: List.generate(50, (index) {
          final src = "https://picsum.photos/900/900?random=$index";
          return GestureDetector(
            onTap: () {
              // Navigator.of(context).push(MaterialPageRoute(
              //   builder: (BuildContext context) {
              //     return ImageDetail(
              //       src: src,
              //     );
              //   },
              // ));

              Navigator.of(context).push(PageRouteBuilder(
                // transitionDuration: const Duration(seconds: ),
                pageBuilder: (BuildContext context, Animation<double> animation,
                    Animation<double> secondaryAnimation) {
                  return FadeTransition(
                    opacity: animation,
                    child: ImageDetail(src: src),
                  );
                },
              ));
            },
            child: Hero(
                tag: src,
                child: Image.network(
                  src,
                  fit: BoxFit.fitWidth,
                )),
          );
          return Container(
            color: Color.fromARGB(255, random.nextInt(256), random.nextInt(256),
                random.nextInt(256)),
          );
        }),
      ),
    );

    //-------------------------------sliver--------------------------------------
    // return CustomScrollView(
    //   slivers: [
    //     SliverSafeArea(
    //       sliver: SliverPadding(
    //         padding: const EdgeInsets.all(8),
    //         sliver: SliverGrid(
    //           gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
    //               crossAxisCount: 2,
    //               crossAxisSpacing: 8,
    //               mainAxisSpacing: 8,
    //               childAspectRatio: 2,
    //             ),
    //             delegate: SliverChildBuilderDelegate(
    //                 (BuildContext ctx, int i) {
    //                   return Container(
    //                     color: Color.fromARGB(255, random.nextInt(256), random.nextInt(256), random.nextInt(256)),
    //                   );
    //                 },
    //               childCount: 20,
    //             ),
    //         ),
    //       ),
    //     ),
    //   ],
    // );

    // return CustomScrollView(
    //   slivers: [
    //     SliverAppBar(
    //       pinned: true,
    //       backgroundColor: Colors.pink,
    //       expandedHeight: 200,
    //       flexibleSpace: FlexibleSpaceBar(
    //         title: Text("Custom View"),
    //         background: Image.asset(
    //           "assets/images/polor-light.jpg",
    //           fit: BoxFit.fitWidth,
    //         ),
    //       ),
    //     ),
    //     Container(
    //       // height: 300,
    //       child: SliverGrid(
    //         gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
    //           crossAxisCount: 2,
    //           crossAxisSpacing: 8,
    //           mainAxisSpacing: 8,
    //           childAspectRatio: 2,
    //         ),
    //         delegate: SliverChildBuilderDelegate(
    //           (BuildContext ctx, int i) {
    //             return Container(
    //               color: Color.fromARGB(255, random.nextInt(256),
    //                   random.nextInt(256), random.nextInt(256)),
    //             );
    //           },
    //           childCount: 20,
    //         ),
    //       ),
    //     ),
    //     SliverList(
    //       delegate: SliverChildBuilderDelegate((BuildContext ctx, int index) {
    //         return ListTile(
    //           title: Text("Item ${index}"),
    //         );
    //       }),
    //     )
    //   ],
    // );
  }
}

class ImageDetail extends StatelessWidget {
  final String src;
  const ImageDetail({Key? key, required this.src}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Hero(tag: src, child: Image.network(src)),
        ),
      ),
    );
  }
}

class ListEventWidget extends StatefulWidget {
  const ListEventWidget({Key? key}) : super(key: key);

  @override
  State<ListEventWidget> createState() => _ListEventWidgetState();
}

class _ListEventWidgetState extends State<ListEventWidget> {
  var _controller = ScrollController(initialScrollOffset: 300);
  var _back = false;

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      // print("controller scroll..... ${_controller.offset}");
    });
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener(
      onNotification: (ScrollNotification notify) {
        if (notify is ScrollStartNotification) {
          print("Start scroll");
        } else if (notify is ScrollEndNotification) {
          print("end scroll");
        } else if (notify is ScrollUpdateNotification) {
          print("update scrol  ${notify.metrics.pixels}");
        }

        return true;
      },
      child: ListView(
        controller: _controller,
        children: List.generate(
          100,
          (index) {
            return Text("Item --->${index}");
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    _controller.dispose();
  }
}
