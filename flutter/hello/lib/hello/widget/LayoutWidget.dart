import 'package:flutter/material.dart';

class LayoutWidget extends StatelessWidget {
  const LayoutWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StackWidget();
  }
}

class StackWidget extends StatefulWidget {
  const StackWidget({Key? key}) : super(key: key);

  @override
  State<StackWidget> createState() => _StackWidgetState();
}

class _StackWidgetState extends State<StackWidget> {
  var _favor = false;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset("assets/images/polor-light.jpg"),
        Positioned(
          bottom: 0,
          right: 0,
          left: 0,
          child: Container(
            padding: EdgeInsets.all(0),
            margin: EdgeInsets.all(0),
            color: Colors.green,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text(
                  "海贼王-勇者无敌",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
                IconButton(
                  splashRadius: 0.0001,
                  padding: EdgeInsets.zero,
                  constraints: const BoxConstraints(minWidth: 40, maxWidth: 40),
                  onPressed: () {
                    setState(() {
                      _favor = !_favor;
                    });
                  },
                  icon: Icon(
                    Icons.favorite,
                    color: _favor ? Colors.white : Colors.pink,
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class ColumWidget extends StatelessWidget {
  const ColumWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 200,
          height: 200,
          alignment: Alignment.centerLeft,
          margin: const EdgeInsets.all(10),
          child: Icon(
            Icons.pets,
            size: 100,
            color: Colors.red,
          ),
          decoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.circular(100),
              boxShadow: const [
                BoxShadow(
                  color: Colors.orange,
                  offset: Offset(10, 10),
                  spreadRadius: 5,
                  blurRadius: 5,
                ),
              ],
              border: Border.all(
                width: 5,
                color: Colors.purple,
              )),
        )
      ],
    );
  }
}
