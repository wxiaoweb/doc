import 'package:flutter/material.dart';
import 'package:hello/hello/widget/test.dart';
import 'package:provider/provider.dart';

import 'app.dart';
import 'hello/animation_widget.dart';
import 'hello/count.dart';
import 'hello/fit_widget.dart';
import 'hello/gesture.dart';
import 'hello/router/router_app.dart';
import 'hello/widget/state_widget.dart';

void main() {
  runApp(
      FitWidgetApp()
  );
}



