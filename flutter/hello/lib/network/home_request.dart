
import 'http_request.dart';
import '../models/home_model.dart';

class HomeRequest {

  static Future<List<MovieItem> > getMovieTopList(int start, int count) async {
    final url = "https://movie.douban.com/j/chart/top_list?type=11&interval_id=100%3A90&action=&start=$start&limit=$count";
    final result = await HttpRequest.request(url);

    List<MovieItem> movies = [];
    for (var sub in result) {
      movies.add(MovieItem.fromJson(sub));
    }

    return movies;
  }
}