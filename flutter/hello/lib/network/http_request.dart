import 'package:dio/dio.dart';
import './http_option.dart';

class HttpRequest {
  static final BaseOptions baseOptions = BaseOptions(
    // baseUrl: "",
    connectTimeout: timeout,
  );

  static final Dio dio = Dio(baseOptions);

  static Future<T> request<T>(
    String url, {
    String method = "get",
    Map<String, dynamic>? params,
    Interceptor? interceptor,
  }) async {

    final option = Options(method: method);
    Interceptor dinter = InterceptorsWrapper(
      onRequest: (option, handler) {
        // return option;
      },

      onResponse: (response, handler) {
        // return response;
      },

      onError: (err, handler) {
        // return err;
      }
    );

    try {
      Response response =
          await dio.request(url, queryParameters: params, options: option);
      return response.data;
    } on DioException catch (e) {
      return Future.error(e);
    }
  }
}
