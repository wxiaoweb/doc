import 'package:flutter/material.dart';

import './core/router/fv_router.dart';
import './ui/theme/fv_theme.dart';
import 'core/common/screen_fit.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    ScreenFit.initialize();

    return MaterialApp(
      title: 'Favor Cake',
      theme: FvTheme.norTheme,
      // theme: Theme.of(context).copyWith(
      //   brightness: Brightness.light,
      //   primaryColor: Colors.blue,
      // ),
      // theme: ThemeData(
      //   brightness: Brightness.light,
      //
      //   primarySwatch: Colors.blue, // header bottom and system widget
      //
      // ),
      initialRoute: FvRouter.initialRoute, // initialRoute 或者 home，必须有一个才行
      routes: FvRouter.routes,
      onGenerateRoute: FvRouter.generateRoute,
      onUnknownRoute: FvRouter.unknownRoute,
    );
  }
}
