import 'dart:ui';

class ScreenFit {
  static double physicalWidth = 1;
  static double physicalHeight = 1;
  static double dpr = 1;
  static double screenWidth = 1;
  static double screenHeight = 1;
  static double rpx = 1;
  static double px = 1;

  static void initialize({double standard = 750}) {
    physicalWidth = window.physicalSize.width;
    physicalHeight = window.physicalSize.height;

    dpr = window.devicePixelRatio;

    screenWidth = physicalWidth / dpr;
    screenHeight = physicalHeight / dpr;

    rpx = screenWidth / standard;
    px = screenWidth / standard * 2;
  }

  static double setRpx(double size) {
    return rpx * size;
  }

  static double setPx(double size) {
    return px * size;
  }
}
