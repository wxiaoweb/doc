
import '../common/screen_fit.dart';

extension DoubleFit on double {
  double get px {
    return ScreenFit.setPx(this);
  }

  double get rpx {
    return ScreenFit.setRpx(this);
  }
}