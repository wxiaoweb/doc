import '../common/screen_fit.dart';

extension IntFit on int {
  double get px {
    return ScreenFit.setPx(toDouble());
  }

  double get rpx {
    return ScreenFit.setRpx(toDouble());
  }
}