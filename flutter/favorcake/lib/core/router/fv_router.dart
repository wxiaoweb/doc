import 'package:flutter/cupertino.dart';

import '../../ui/pages/main/main.dart';
import '../../ui/pages/favor/favor.dart';
import '../../ui/pages/home/home.dart';

class FvRouter {
  static const String initialRoute = MainScreen.routeName;

  static final Map<String, WidgetBuilder> routes = {
    MainScreen.routeName: (ctx) => const MainScreen(),
    HomeScreen.routeName: (ctx) => const HomeScreen(),
    FavorScreen.routeName: (ctx) => const FavorScreen(),
  };

  static final RouteFactory generateRoute = (RouteSettings settings) {
    return null;
  };
  static final RouteFactory unknownRoute = (RouteSettings settings) {
    return null;
  };
}