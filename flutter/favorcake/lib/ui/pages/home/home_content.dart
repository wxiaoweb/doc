import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../core/model/category_model.dart';
import '../../../core/services/json_parse.dart';

import '../../../core/extensions/int_ext.dart';

class HomeContent extends StatefulWidget {
  const HomeContent({Key? key}) : super(key: key);

  @override
  State<HomeContent> createState() => _HomeContentState();
}

class _HomeContentState extends State<HomeContent> {
  List<CategoryModel> _categoris = [];
  @override
  void initState() {
    super.initState();

    JsonParse.getCategoryData().then((value) {
      setState(() {
        _categoris = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      padding: EdgeInsets.all(20.px),
      itemCount: _categoris.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 20.px,
        mainAxisSpacing: 20.px,
        childAspectRatio: 1.5,
      ),
      itemBuilder: (BuildContext context, int index) {
        // print("index--> $index");
        final bgColor = _categoris[index].cColor;
        final title = _categoris[index].title;

        return Container(
            decoration: BoxDecoration(
              color: bgColor!,
              borderRadius: BorderRadius.circular(12),
              gradient: LinearGradient(
                colors: [
                  bgColor.withOpacity(.5),
                  bgColor,
                ]
              )
            ),

          alignment: Alignment.center,
          child: Text(title!, style: const TextStyle(fontWeight: FontWeight.bold),),
        );
      },
    );
  }
}
