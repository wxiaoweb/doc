import 'package:flutter/material.dart';

import 'home_content.dart';

class HomeScreen extends StatelessWidget {
  static const routeName = "/home";


  const HomeScreen({Key? key}):super(key:key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("美食广场"),
      ),

      body: HomeContent(),
    );
  }
}
