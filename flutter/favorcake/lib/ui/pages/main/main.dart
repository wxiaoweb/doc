import 'package:flutter/material.dart';

import './initialize.dart';

class MainScreen extends StatefulWidget {
  static const routeName = "/main";

  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _idx = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _idx,
        children: pages,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _idx,
        items: items,
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (index) {
          setState(() {
            _idx = index;
          });
        },
      ),
    );
  }
}
