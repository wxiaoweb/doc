import 'package:favorcake/ui/pages/favor/favor.dart';
import 'package:favorcake/ui/pages/home/home.dart';
import 'package:flutter/material.dart';

final List<Widget> pages = [
  HomeScreen(),
  FavorScreen(),
];

final List<BottomNavigationBarItem> items = [
  const BottomNavigationBarItem(icon: Icon(Icons.home), label: "首页"),
  const BottomNavigationBarItem(icon: Icon(Icons.favorite), label: "收藏"),
];
