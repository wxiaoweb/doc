import 'package:flutter/material.dart';

class FvTheme {
  static const double smallFontSize = 16;
  static const double normalFontSize = 22;
  static const double largeFontSize = 24;

  static const Color norTextColors = Colors.black;
  static final ThemeData norTheme = ThemeData(
    primarySwatch: Colors.pink,
    canvasColor: const Color.fromARGB(255, 254, 222, 1),
    textTheme: const TextTheme(
      // bodyMedium: TextStyle(fontSize: normalFontSize, color: norTextColors),
      // displaySmall: TextStyle(fontSize: smallFontSize, color: norTextColors),
      // displayMedium: TextStyle(fontSize: normalFontSize, color: norTextColors),
      // displayLarge: TextStyle(fontSize: largeFontSize, color: norTextColors),
    ),
  );


  static const Color darkTextColors = Colors.green;
  static final ThemeData darkTheme = ThemeData(
    primarySwatch: Colors.grey,
    textTheme: const TextTheme(
      // bodyMedium: TextStyle(fontSize: normalFontSize, color: darkTextColors),
    ),
  );
}